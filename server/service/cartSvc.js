let deduplicate = (rows) => {
  //Deduplicate logic
  const unique = [...new Map(rows.map((item) => [item["id"], item])).values()];
  return unique;
};

module.exports = deduplicate;
