var sqlite3 = require("sqlite3").verbose();
var path = require("path");

let updatedb = () => {
  let db = new sqlite3.Database(path.join(__dirname, "shop.db"), (err) => {
    if (err) {
      console.log(err.message);
      throw err;
    } else {
      let sql = `UPDATE product SET obsoleted=(case when (type='B' AND price<=20) OR type='A' then 1 else 0 end)`;

      db.run(sql, function (err) {
        if (err) {
          return console.error(err.message);
        }
        console.log(`Row(s) updated: ${this.changes}`);
      });

      // close the database connection
      db.close();
    }
  });
};

module.exports = updatedb;
