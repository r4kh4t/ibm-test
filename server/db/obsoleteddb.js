var sqlite3 = require("sqlite3").verbose();
var path = require("path");
const updatedb = require("./updatedb");

let obsoleteddb = () => {
  let db = new sqlite3.Database(path.join(__dirname, "shop.db"), (err) => {
    if (err) {
      console.log(err.message);
      throw err;
    } else {
      db.run(`alter table product add column obsoleted int`, function (err) {
        updatedb();
      });
    }
  });
};

module.exports = obsoleteddb;
